# README #

SMS Tools for calculating multi part message lengths and determining encodings.

 * MIT licensed
 * Copyright 2015 Solvam Corporation Ltd

### How do I get set up? ###

This library is composerified but not on packagist yet. Use a repository entry in your composer.json file.

### Information ###

Currently this library can calculate how many SMS message parts a given string will require.

Calculating this is not as easy as it seems, as GSM encodes in either 7-bit GSM 03.38 encoding, or UCS-2 if *any* of the characters present cannot be encoded in 7-bit.

7-bit GSM encoding is tricky to calculate too, as it has a 14-bit per character extended set that needs calculation.

In addition, carriers and phones now support UTF-16, and 32-bit characters, despite being sent as UCS-2 over the wire. SMS messages with wide chars determine multipart length based on number of 16-bit characters, so a >16-bit character in this encoding adds two characters to the total count for calculating number of SMS parts.

Part lengths work as follows. For 7-bit messages:

 * 1 part: Up to and including 160 7-bit characters
 * 2 parts: 161 to 306 7-bit characters inclusive
 * 3 parts: 307 to 459 7-bit characters inclusive

For UCS-2 encoding:

 * 1 part: Up to and including 70 16-bit characters
 * 2 parts: 71 to 134 7-bit characters inclusive
 * 3 parts: 135 to 201 7-bit characters inclusive

### Usage ###

```
#!php

use Solvam\SmsTools\GsmSmsEncoding;

$str = 'my string...';
echo GsmSmsEncoding::multipartLength($str, 'UTF-8'); // 1
```